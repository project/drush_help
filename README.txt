CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

To see the drush commands help you need to use drush help COMMAND_NAME from the
command line, but if you want to see this information in Drupal you'll can't do
it. This module improves the module help page showing information about the
module drush commands. The drush command help section only will be added if the
module for which you are viewing the help have drush commands defined.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/drush_help

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/search/drush_help


REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/docs/8/extending-drupal-8/installing-modules
   for further information.


CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration.


MAINTAINERS
-----------

Current maintainers:
 * Adrian Cid Almaguer (adriancid) - https://www.drupal.org/u/adriancid
